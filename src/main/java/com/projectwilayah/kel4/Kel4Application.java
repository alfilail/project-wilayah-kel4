package com.projectwilayah.kel4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kel4Application {

	public static void main(String[] args) {
		SpringApplication.run(Kel4Application.class, args);
	}

}
